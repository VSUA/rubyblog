class ArticlesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  before_action :set_article, only: %i[show edit update destroy]
  # before_action :authorize_user!, only: %i[edit update destroy]

  def index
    @articles = Article.ordered.with_authors.paginate(page: params[:page], per_page: 2).where('title LIKE ?', "#{params[:search]}%")
    #@articles = Article.all
  end

  def show

  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    @article.author = current_user
    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    authorize @article
  end

  def update
    authorize @article

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    authorize @article

    @article.destroy

    redirect_to root_path
  end

  private
  def article_params
    params.require(:article).permit(:title, :body, :status, :image)
  end

  def set_article
    @article = Article.find(params[:id])
  end

  # def authorize_user!
  #   return if @article.author_id == current_user.id
  #
  #   redirect_to :articles,
  #               alert: "You are not allowed to perform this acrion"
  # end
end